
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgproc.hpp"

#include "sift.h"

#include <memory>

#include <iostream>

namespace opencv {

std::vector<float> GetImageSiftFeature(const std::string& image_filename) {
    using namespace cv;
    const int kNumKeyPoints = 2;

    Mat raw_img = imread(image_filename);
    Mat gray_img;
    
    cvtColor(raw_img, gray_img, COLOR_BGR2GRAY);

    std::vector<KeyPoint> image_keypoints;
    Ptr<SIFT> sift_extractor = SIFT::create(kNumKeyPoints, 3, 0.04, 10, 1.6, CV_32F);
    sift_extractor->detect(gray_img, image_keypoints, Mat());
    
    Mat image_descriptions;
    sift_extractor->compute(gray_img, image_keypoints, image_descriptions);

    #ifdef DEBUG
    std::cout << "image description size: " << image_descriptions.size() << std::endl;
    std::cout << "description glance: ";
    for (int i=0; i<10; ++i)
        std::cout << image_descriptions.at<float>(i) << ", ";
    std::cout << std::endl;
    #endif

    std::vector<float> sift_feature(256);
    std::memcpy(sift_feature.data(), image_descriptions.data, sizeof(float)*128*kNumKeyPoints);

    return sift_feature;
}

}   // sift

