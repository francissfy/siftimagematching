#include "sift.h"

#include <iostream>

int main(const int argc, const char* argv[]) {
    const char* filename = argv[1];
    std::cout << "filename: " << filename << std::endl;

    std::vector<float> feature = sift::GetImageSiftFeature(filename);

    std::cout << "feature size: " << feature.size() << " glance: ";
    for (int i=0; i<10; ++i)
        std::cout << feature[i] << ", ";
    std::cout << std::endl; 
    return 0;
}