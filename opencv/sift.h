#ifndef OPENCV_SIFT_H_
#define OPENCV_SIFT_H_

#include <string>
#include <vector>

namespace opencv {

std::vector<float> GetImageSiftFeature(const std::string& image_filename);



}

#endif      // OPENCV_SIFT_H_
