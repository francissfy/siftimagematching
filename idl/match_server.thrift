enum RetCode {
    OK = 1,
    ERROR = 2
}

struct ImageData {
    1: string name,
    2: binary bin   
}

struct UploadResponse {
    1: RetCode code,
    2: string msg
}

service ImageMatchService {
    UploadResponse UploadImage(1: ImageData img_data)
}