#include "hnswlib.h"

#include <vector>
#include <memory>
#include <iostream>

using idx_t = hnswlib::labeltype;   // size_t

int main(const int argc, const char* argv[]) {
    const int d = 4;
    idx_t n = 100;
    idx_t nq = 10;
    size_t k = 10;

    std::vector<float> data(n*d);
    std::vector<float> query(nq*d);

    std::mt19937 rng;
    rng.seed(47);
    std::uniform_real_distribution<double> distrib;

    for (idx_t i=0; i<n*d; ++i) {
        data[i] = distrib(rng);
    }

    for (idx_t i=0; i<nq*d; ++i) {
        query[i] = distrib(rng);
    }

    hnswlib::L2Space space(d);
    auto alg_hnsw = std::unique_ptr<hnswlib::AlgorithmInterface<float>>(new hnswlib::HierarchicalNSW<float>(&space, 2*n));


    for (size_t i=0; i<n; ++i) {
        alg_hnsw->addPoint(data.data()+d*i, i);
    }

    for (size_t j=0; j<nq; ++j) {
        const void* p = query.data() + j*d;

        auto gd = alg_hnsw->searchKnn(p, k);
        auto res = alg_hnsw->searchKnnCloserFirst(p, k);
        
        while (!gd.empty()) {
            auto& pair = gd.top();
            std::cout << "score: " << pair.first << " label: " << pair.second << std::endl;
            gd.pop();
        }

    }

    return 0;
}