#ifndef DB_STORAGE_H_
#define DB_STORAGE_H_

#include <memory>
#include <string>

namespace db {

void UploadImage(const std::string& img_name, const uint8_t* src, const uint32_t binary_size);
    
}   // db

#endif