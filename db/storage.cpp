#include "storage.h"

#include "bsoncxx/json.hpp"
#include "mongocxx/client.hpp"
#include "mongocxx/stdx.hpp"
#include "mongocxx/uri.hpp"
#include "mongocxx/instance.hpp"
#include "bsoncxx/builder/stream/helpers.hpp"
#include "bsoncxx/builder/stream/document.hpp"
#include "bsoncxx/builder/stream/array.hpp"

namespace db {

static mongocxx::instance instance{};
const char* kReplicaStr = "mongodb+srv://sfy:frank2325Frank@cluster0.usxw0.mongodb.net/testdb?retryWrites=true&w=majority";


void UploadImage(const std::string& img_name, const uint8_t* src, const uint32_t binary_size) {
    auto img = bsoncxx::types::b_binary{bsoncxx::binary_sub_type::k_binary, binary_size, src};

    auto builder = bsoncxx::builder::stream::document();
    bsoncxx::document::value doc_value = builder 
        << "name" << img_name 
        << "bin" << img
    << bsoncxx::builder::stream::finalize;

    mongocxx::client client(mongocxx::uri{kReplicaStr});
    mongocxx::database db = client["test_db"];
    mongocxx::collection col = db["test_col"];

    auto insert_ret = col.insert_one(doc_value.view());
    
    if (!insert_ret) {
        fprintf(stderr, "insert one error");
        return;
    }

    return;
}

}