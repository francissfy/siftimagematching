#ifndef HANDLER_H_
#define HANDLER_H_

#include "ImageMatchService.h"
#include "match_server_types.h"
#include "db/storage.h"



class ImageMatchServiceHandler: virtual public ImageMatchServiceIf {
public:

ImageMatchServiceHandler() {
    // Your initialization goes here
}

void UploadImage(UploadResponse& _return, const ImageData& img_data) {
    const char* bin_data = img_data.bin.data();
    const size_t bin_size = img_data.bin.size();
    const std::string& img_name = img_data.name;

    db::UploadImage(img_name, reinterpret_cast<const uint8_t*>(bin_data), static_cast<uint32_t>(bin_size));

    _return.code = RetCode::OK;
    _return.msg = "ok";

    return;
}

};


#endif      // HANDLER_H_

